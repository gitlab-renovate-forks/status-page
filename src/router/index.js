import Vue from 'vue';
import VueRouter from 'vue-router';
import PageNotFound from '~/views/404.vue';
import IncidentDetailsPage from '~/views/details.vue';
import IncidentListPage from '~/views/list.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'list',
    component: IncidentListPage,
  },
  {
    path: '/:link',
    name: 'details',
    component: IncidentDetailsPage,
  },
  { path: '*', component: PageNotFound },
];

export const createRouter = () => new VueRouter({ routes });
